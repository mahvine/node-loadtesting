'use strict';

const express = require('express');
const SocketServer = require('ws').Server;
const path = require('path');

const PORT = process.env.PORT || 3001;
const INDEX = path.join(__dirname, 'index.html');

const server = express()
    .get('/', (req, res) => res.sendFile(INDEX) )
    .get('/hello', function (req, res) { res.send('Hello World!') })
    .get('/delayedHello', function (req, res) { 
        var random = Math.random(3,10) * 1000;
        setTimeout(()=>{
            res.send('THIS IS NOT Hello World!');
        },random);
    })
    .listen(PORT, () => console.log(`Listening on ${ PORT }`));

const wss = new SocketServer({ server:server, path:"/websocket" });

wss.on('connection', (ws) => {
  console.log('Client connected');
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });
  ws.on('close', () => console.log('Client disconnected'));
});

setInterval(() => {
  wss.clients.forEach((client) => {
    client.send(new Date().toTimeString());
  });
}, 1000);